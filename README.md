README
======

This project holds the assets for the app build. We are using Apache 
Cordova (https://cordova.apache.org/) to create the application for iOS 
and Android. The web assets (HTML, JS, CSS etc.) are maintained in a 
separate project (https://bitbucket.org/outstories/outstoriesjs) since 
they are shared with the map that appears on the OutStories website and 
are copied over as part of the build process. The assets are committed 
to git in this project so we can track what was deployed with a version 
of the app, but the assets in the OutstoriesJs project should be seen as
the definitive version and updates should be made there and copied over.

We are using `npm` and `grunt` to manage the project.

After cloning the project run the following to install npm dependencies:

```
    sudo npm install
```

Install Cordova:

```
    sudo npm install -g cordova
```

Install platform(s):
```
    cordova platform add ios
    cordova platform add android
```

Install Cordova plugins:

```
    cordova plugin add cordova-plugin-splashscreen
    cordova plugin add cordova-plugin-geolocation
    cordova plugin add cordova-plugin-statusbar
```

Create a `config.properties` file that has a `srcDir` property that 
points to the app assets in the OutStoriesJs project:

```
srcDir=/home/mike/workspaces/outstoriesjs/platform/ios_app
```

The following will copy the assets into the project:

```
    grunt default
```

To build the iOS project:

```
    cordova build ios
```

Note: You need to select the supported orientations in the XCode
project since we can't differentiate between iPhone and iPad. 
There isn't much space for landscape on the iPhone, so select
'portrait' for iPhone and all for iPad.