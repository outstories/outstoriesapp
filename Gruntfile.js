module.exports = function (grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        app: {
            name: 'outstoriesapp'
        },
        properties: {
            cfg: 'config.properties'
        },
        clean: {
            clean_assets: ['www/*.html', 'www/css/*', 'www/image/*', 'www/js/*']
        },
        copy: {
            copy_assets: {
                files: [
                    {
                        expand: true,
                        src: ['<%= cfg.srcDir %>/css/**'],
                        dest: 'www/css',
                        filter: 'isFile',
                        flatten: true
                    },
                    {
                        expand: true,
                        src: ['<%= cfg.srcDir %>/image/**'],
                        dest: 'www/image',
                        filter: 'isFile',
                        flatten: true
                    },
                    {
                        expand: true,
                        src: ['<%= cfg.srcDir %>/js/**'],
                        dest: 'www/js',
                        filter: 'isFile',
                        flatten: true
                    },
                    {
                        expand: true,
                        src: ['<%= cfg.srcDir %>/index.html'],
                        dest: 'www/',
                        filter: 'isFile',
                        flatten: true
                    }
                ]
            }
        }

    });

    // we can use a properties file
    grunt.loadNpmTasks('grunt-properties-reader');

    // clean
    grunt.loadNpmTasks('grunt-contrib-clean');

    // copy our files into place
    grunt.loadNpmTasks('grunt-contrib-copy');

    // default task(s).
    grunt.registerTask('default', ['properties', 'clean:clean_assets', 'copy:copy_assets']);

};