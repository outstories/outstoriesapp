var app = {

    osb_map: null,
    init: function () {

        // map box listener
        jQuery('#osb-map-box').on('click', function (e) {

            // don't click through
            e.preventDefault();

            // hide the main page, show the map
            jQuery('#osb-main-page-wrapper').hide();
            jQuery('#osb-map-page-wrapper').show();

            // show the back button and add a listener (return to home page)
            jQuery('p.osb-back').show().off('click').on('click', function (e) {
                e.preventDefault();
                osb.stopMediaPlaying();
                if (jQuery("#osb-place-description").is(':visible') === true) {
                    osb.hidePlaceDetails();
                } else {
                    jQuery(this).hide();
                    jQuery('#osb-main-page-wrapper').show();
                    jQuery('#osb-map-page-wrapper').hide();
                }
            });

            // set up map
            if (app.osb_map === null) {
                app.osb_map = L.map('osb-map', {zoomControl: false}).setView([51.450579, -2.594704], 16);
                osb.init(mapper, app.osb_map, osb_layers.layers, null);

                app.location.findLocation();
            }

            jQuery('#osb-place-report').off('click').on('click', function (e) {
                e.preventDefault();
                console.log("Want to report ...");
                var place = jQuery('#osb-place-description-content > h3').text();
                var subject = 'Report: ' + place;
                var body = "Dear admin\n\nI'd like to report an issue with \"" + place +
                    '\"\n\n[Please provide details ...]';
                window.location.href = 'mailto:webadmin@outstoriesbristol.org.uk?subject=' + encodeURIComponent(subject) +
                    '&body=' + encodeURIComponent(body);
            });

        });
        jQuery('#osb-about-box').on('click', function (e) {

            // don't click through
            e.preventDefault();

            // hide the main page, show the map
            jQuery('#osb-main-page-wrapper').hide();
            jQuery('#osb-about-page-wrapper').show();

            // show the back button and add a listener (return to home page)
            jQuery('p.osb-back').show().off('click').on('click', function (e) {
                e.preventDefault();
                jQuery(this).hide();
                jQuery('#osb-main-page-wrapper').show();
                jQuery('#osb-about-page-wrapper').hide();
            });

        });

        jQuery('#osb-settings-box').on('click', function (e) {

            // don't click through
            e.preventDefault();

            // hide the main page, show the map
            jQuery('#osb-main-page-wrapper').hide();
            jQuery('#osb-settings-page-wrapper').show();

            // show the back button and add a listener (return to home page)
            jQuery('p.osb-back').show().off('click').on('click', function (e) {
                e.preventDefault();
                jQuery(this).hide();
                jQuery('#osb-main-page-wrapper').show();
                jQuery('#osb-settings-page-wrapper').hide();
            });

            jQuery('#osb-app-reset').on('click', function () {
                osb.cache.clear();
                jQuery('#osb-reset-msg').text('The application has been reset').show().fadeOut(5000);

            });
        });

    },

    location: {

        watchId: null,
        pollCount: 0,

        findLocation: function () {

            app.location.watchId = navigator.geolocation.watchPosition(app.location.update, app.location.error,
                {enableHighAccuracy: true});
        },

        update: function (position) {
            if (position.coords.accuracy <= 80 || app.location.pollCount === 10) {
                app.location.stopWatch();

                var radius = position.coords.accuracy / 2;
                L.circle([position.coords.latitude, position.coords.longitude], radius).addTo(osb.map.map);

                if (osb.map.markers !== null && osb.map.markers.getBounds().contains([position.coords.latitude, position.coords.longitude])) {
                    osb.map.map.panTo([position.coords.latitude, position.coords.longitude]);
                }

            } else {
                app.location.pollCount++;
            }
        },

        error: function () {
            app.location.stopWatch();
        },

        stopWatch: function () {
            if (app.location.watchId !== null) {
                navigator.geolocation.clearWatch(app.location.watchId);
                app.location.pollCount = 0;
            }
        }

    }

};

app.init();